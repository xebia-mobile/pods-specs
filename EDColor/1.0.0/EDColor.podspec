Pod::Spec.new do |s|
  s.name                      = "EDColor"
  s.version                   = "1.0.0"
  s.summary                   = "A collection of categories and utilities that extend UIColor."
  s.homepage                  = "https://github.com/thisandagain/color"
  s.license                   = 'MIT'
  s.author                    = { "Andrew Sliwinski" => "andrewsliwinski@acm.org", "Daniel Doubrovkine" => "dblock@dblock.org" }
  s.source                    = { :git => "https://github.com/thisandagain/color.git", :tag => "v1.0.0" }
  s.ios.deployment_target     = '6.0'
  s.watchos.deployment_target = '2.0'
  s.source_files              = 'EDColor'
  s.frameworks                = 'UIKit'
  s.requires_arc              = true

  pch = <<-EOS
  #ifdef TARGET_OS_WATCH
    #undef TARGET_OS_IPHONE
    #define TARGET_OS_IPHONE 0
  #endif
EOS
  s.prefix_header_contents = pch
end
