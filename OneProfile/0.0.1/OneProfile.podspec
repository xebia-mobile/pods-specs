Pod::Spec.new do |s|

  s.name         = "OneProfile"
  s.version      = "0.0.1"
  s.summary      = "OneProfile for Louis Vuitton iOS apps."
  s.description  = "OneProfile for Louis Vuitton iOS apps. Currently used for apps such as LiVe and Learning"

  s.homepage     = "https://bitbucket.org/xebia-mobile/lv-oneprofile-mobile"
  s.license      = { :type => "© 2016 Louis Vuitton", :file => "COPYRIGHT" }
  s.author       = { "Simone Civetta" => "viteinfinite@gmail.com" }
  s.platform     = :ios, "8.0"
  s.source       = { :git => "http://bitbucket.org/xebia-mobile/lv-oneprofile-mobile.git", :tag => "0.0.1" }
  s.source_files  = "OneProfile"
  s.exclude_files = "OneProfile/Info.plist"

  s.public_header_files = "OneProfile/**/*.h"

end
