#
#  Be sure to run `pod spec lint ComScore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |s|
  s.name         = "TagCommander"
  s.version      = "3.2.20160422"
  s.summary      = "Enabled marketers to easily add tags on web pages, videos and mobile applications"

  s.homepage     = "http://www.tagcommander.com"
  s.license      = { :type => "Copyright", :text => "" }

  s.author       = s.name

  s.platform     = :ios, "6.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/pod-tagcommander.git", :tag => s.version }

  s.vendored_library = "tc/libTagCommander.a"
  s.source_files = "tc/include/*.h"
  s.requires_arc = true
end