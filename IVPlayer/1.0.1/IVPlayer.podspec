#
#  Be sure to run `pod spec lint ComScore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |s|
  s.name         = "IVPlayer"
  s.version      = "1.0.1"
  s.summary      = "Invisu video player #MEGALOL"

  s.homepage     = "http://google.com?q=faillite"
  s.license      = { :type => "Copyright", :file => "LICENSE" }

  s.author       = "Invisu"

  s.platform     = :ios, "6.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/ivplayer", :tag => s.version }
  s.requires_arc = true

  s.source_files = [ 'IVPlayer/IVPlayer/**/*.{h,m}' ]
end
