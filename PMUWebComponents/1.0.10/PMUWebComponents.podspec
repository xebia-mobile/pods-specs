Pod::Spec.new do |s|
  s.name		  = "PMUWebComponents"
  s.version		  = "1.0.10"
  s.source		  = { :git => "git@github.com:xebia-studio/PMUWebComponents.git",
  		     	      :tag => s.version.to_s }

  s.summary          	  = "PMU web components for iOS native apps"
  s.description           = "Reach PMU web components (GCMV) from iOS native apps"
  s.homepage          	  = "http://www.xebia.fr"
  s.license		  = { :type => "MIT", :text => "MIT" }
  s.author                = 'xebia'

  s.ios.deployment_target = "6.0"
  s.source_files    	  = "#{s.name}/**/*.{h,m}"
  s.prefix_header_file    = "#{s.name}/#{s.name}.h"
  s.resources 		  = ["#{s.name}/**/*.xib", "#{s.name}/Assets.xcassets"]
  s.requires_arc 	  = true

end
