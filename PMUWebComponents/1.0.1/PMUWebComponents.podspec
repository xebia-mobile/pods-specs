Pod::Spec.new do |s|
  s.name		  = "PMUWebComponents"
  s.version		  = "1.0.1"
  s.source		  = { :git => "git@github.com:xebia-studio/PMUWebComponents.git",
  		     	    :tag => s.version.to_s }

  s.summary          	  = "PMU web components for iOS native apps"
  s.description           = "Reach PMU web components (GCMV) inside iOS native apps"
  s.homepage          	  = "http://xebia.fr",
  s.license		  = { :type => "MIT", :file => "LICENSE" }
  s.author                = 'xebia'

  s.ios.deployment_target = "6.0"
  s.source_files    	  = "#{s.name}/**/*.{h,m}"
  s.prefix_header_file    = "#{s.name}/#{s.name}.h"
  s.resources 		  = ["#{s.name}/**/*.xib", "#{s.name}/Assets.xcassets"]
  s.requires_arc 	  = true

end
