Pod::Spec.new do |s|
  s.name         = "Azetone"
  s.version      = "3.6"
  s.summary      = "Azetone is a new leader in Mobile A/B Testing and Personalization. We deliver to Mobile App Marketers and Developers unmatched agility, efficiency and relevance with their iOS and Android Apps by providing a unique platform to visually analyze, improve and personalize the Mobile User Experience."

  s.homepage     = "http://www.azetone.com"
  s.license      = { :type => "Copyright", :text => "" }

  s.author       = s.name

  s.platform     = :ios, "5.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/pod-azetone.git", :tag => s.version }

  s.libraries = 'sqlite3'
  s.vendored_frameworks = 'Azetone.framework'
  s.requires_arc = true
end