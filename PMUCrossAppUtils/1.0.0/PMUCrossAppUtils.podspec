Pod::Spec.new do |s|
  s.name		               = "PMUCrossAppUtils"
  s.version		             = "1.0.0"
  s.source		             = { :git => "https://github.com/xebia-studio/PMUCrossAppUtils.git",
  		     	        :tag => s.version.to_s }

  s.summary                = "PMU cross app utils for iOS native apps"
  s.description            = "Range of tools making cross app interactions simpler for iOS native apps"
  s.homepage               = "http://www.xebia.fr"
  s.license		             = { :type => "MIT", :text => "MIT" }
  s.author                 = 'xebia'

  s.ios.deployment_target  = "6.0"
  s.source_files    	     = "#{s.name}/**/*.{h,m}"
  s.prefix_header_file     = "#{s.name}/#{s.name}.h"
  s.requires_arc 	         = true

end
