#
#  Be sure to run `pod spec lint ComScore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |s|
  s.name         = "Capptain"
  s.version      = "1.16.2.1"
  s.summary      = "Analytics tool"

  s.homepage     = "https://app.capptain.com/doc/SDK/iOS/index.html"
  s.license      = { :type => "Copyright", :file => "LICENSE" }

  s.author       = "Capptain"

  s.platform     = :ios, "6.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/pod-capptain", :tag => s.version }
  s.requires_arc = false

  s.vendored_libraries = ['CapptainReach/libreach.a', 'CapptainSDK/libcapptain.a']
  s.source_files = ['CapptainSDK/**/*.{h,m}', 'CapptainReach/**/*.{h,m}']
  s.resource = 'CapptainReach/res/*'

  s.weak_frameworks = 'UIKit', 'AdSupport'
  s.frameworks = 'SystemConfiguration', 'CoreTelephony', 'CFNetwork', 'CoreLocation'
  s.libraries = 'xml2' 
end
