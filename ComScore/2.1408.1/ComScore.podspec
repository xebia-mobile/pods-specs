#
#  Be sure to run `pod spec lint ComScore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |s|
  s.name         = "ComScore"
  s.version      = "2.1408.1"
  s.summary      = "Application Tag for iOS"

  s.homepage     = "https://bitbucket.org/xebia-mobile/comscore"
  s.license      = { :type => "Copyright", :text => "" }

  s.author       = "ComScore"

  s.platform     = :ios, "6.0"

  s.source       = { :git => s.homepage, :tag => s.version }


  s.vendored_library = 'comScore/libcomScore.a'
  s.source_files = 'comScore/headers/*.h'
end
