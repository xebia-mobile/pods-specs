#
#  Be sure to run `pod spec lint ComScore.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.

Pod::Spec.new do |s|
  s.name         = "LiveServer"
  s.version      = "1.5.2"
  s.summary      = "OpenBet live server"

  s.homepage     = "https://www.openbet.com"
  s.license      = { :type => "Copyright", :file => "LICENSE" }

  s.author       = "OpenBet"

  s.platform     = :ios, "6.0"

  s.source       = { :git => "https://bitbucket.org/xebia-mobile/pod-liveserver", :tag => s.version }
  s.requires_arc = false

  s.source_files = [ 'public/*.h' ]
  s.vendored_library = 'libliveserv-ios.a'
end
